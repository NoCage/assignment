##create a python script (numpy_temp.py) to perform the following tasks:

import openpyxl
import numpy as np
import sqlite3
import matplotlib.pyplot as plt
#wb = openpyxl.load_workbook('WorldTemperature.xlsx')

#create sheet named Comparison
#sheet = wb.create_sheet('Comparison')

#wb.save('WorldTemperature.xlsx')

connection = sqlite3.connect('assignment.db')
cursor = connection.cursor()

##mean temperature of Australia states for each year
sql = 'select date,avg(AverageTemperature),state from GlobalLandTemperaturesByState where country = "Australia"  group by state,strftime("%Y",date)'
result = cursor.execute(sql)

date=[]
meanTemp=[]
state=[]
for row in result:
    date.append(row[0])
    meanTemp.append(row[1])
    state.append(row[2])

for i in range(0,len(date)):
    print(date[i],meanTemp[i],state[i])

nresult = np.array([date,meanTemp,state])



print(nresult)


##mean temperature of Australia for each year
sql1 = 'select date,avg(AverageTemperature) from GlobalLandTemperaturesByCountry where country = "Australia" group by country,strftime("%Y",date)'
result1 = cursor.execute(sql1)
date1=[]
avgTemp1=[]
for row in result:
    date1.append(row[0])
    avgTemp1.append(row[1])


dif=[]
for i in range(1,len(date1)):
    dif.append(avgTemp1[i]-avgTemp1[i-1])

nresult1 = np.array([date,meanTemp,state])

print(nresult1)



### differences between each state and the national date for each year
# get data for each state every year

# get national data every year

# get the differece between

#national temperature
N = []
sql2 = 'select date, AverageTemperature,country from GlobalLandTemperaturesByCountry where country = "Australia" and CAST(strftime("%Y",date) as decimal) >= 1855 group by country,strftime("%Y",date) '
result2 = cursor.execute(sql2)
for row in result2:
    N.append(row[1])

#
sqlWA =  'select date,AverageTemperature ,State from GlobalLandTemperaturesByState where country = "Australia"  and state = "Western Australia" and CAST(strftime("%Y",date) as decimal) >= 1855 group by state,strftime("%Y",date)'
sqlQL = 'select date,AverageTemperature ,State from GlobalLandTemperaturesByState where country = "Australia"  and state = "Queensland" and CAST(strftime("%Y",date) as decimal) >= 1855 group by state,strftime("%Y",date)'
sqlTM = 'select date,AverageTemperature ,State from GlobalLandTemperaturesByState where country = "Australia"  and state = "Tasmania" and CAST(strftime("%Y",date) as decimal) >= 1855 group by state,strftime("%Y",date)'
sqlNT = 'select date,AverageTemperature ,State from GlobalLandTemperaturesByState where country = "Australia"  and state = "Northern Territory" and CAST(strftime("%Y",date) as decimal) >= 1855 group by state,strftime("%Y",date)'
sqlSA = 'select date,AverageTemperature ,State from GlobalLandTemperaturesByState where country = "Australia"  and state = "South Australia" and CAST(strftime("%Y",date) as decimal) >= 1855 group by state,strftime("%Y",date)'
sqlNSW = 'select date,AverageTemperature ,State from GlobalLandTemperaturesByState where country = "Australia"  and state = "New South Wales" and CAST(strftime("%Y",date) as decimal) >= 1855 group by state,strftime("%Y",date)'
sqlVT = 'select date,AverageTemperature ,State from GlobalLandTemperaturesByState where country = "Australia"  and state = "Victoria" and CAST(strftime("%Y",date) as decimal) >= 1855 group by state,strftime("%Y",date)'

WA=[]
QL=[]
TM=[]
NT=[]
SA=[]
NSW=[]
VT=[]
resultWA = cursor.execute(sqlWA)
for row in resultWA:
    WA.append(row[1])

resultQL = cursor.execute(sqlQL)
for row in resultQL:
    QL.append(row[1])

resultTM = cursor.execute(sqlTM)
for row in resultQL:
    TM.append(row[1])

resultNT = cursor.execute(sqlNT)
for row in resultQL:
    NT.append(row[1])

resultSA = cursor.execute(sqlSA)
for row in resultQL:
    SA.append(row[1])

resultNSW = cursor.execute(sqlNSW)
for row in resultNSW:
    NSW.append(row[1])

resultVT = cursor.execute(sqlVT)
for row in resultQL:
    VT.append(row[1])

#print(N)
#print(WA) #1852
#print(QL)  #1855
#print(TM)  #1841
#print(NT)  #1855
#print(SA)  #1841
#print(NSW)  #1841
#print(VT) #1841

#print(len(N),len(WA),len(QL),len(TM),len(NT),len(SA),len(NSW),len(VT))

### use MatPlotLib to plot the difference across years.

WADiff=[]
QLDiff=[]
TMDiff=[]
NTDiff=[]
SADiff=[]
NSWDiff=[]
VTDiff=[]
"""
for i in range(0,len(N)):
    if WA[i] is None :


    if QL[i] is None :
        QL[i]= 0

    if  TM[i] is None:
        TM[i] = 0

    if NT[i] is None:
       NT[i]=0

    if SA[i] is None:
       SA[i] = 0

    if NSW[i] is None:
       NSW[i]= 0

    if VT[i] is None:
       VT[i]=0

    if N[i] is None:
       N[i]=0

    WADiff.append(WA[i] - N[i])
    QLDiff.append(QL[i] - N[i])
    TMDiff.append(TM[i] - N[i])
    NTDiff.append(NT[i] - N[i])
    SADiff.append(SA[i] - N[i])
    NSWDiff.append(NSW[i] - N[i])
    VTDiff.append(VT[i] - N[i])
"""
for i in range(0, len(N)):
    if WA[i] is None or QL[i] is None or TM[i] is None or NT[i] is None or SA[i] is None or NSW[i] is None or VT[i] is None or N[i] is None:
        continue

    WADiff.append(WA[i] - N[i])
    QLDiff.append(QL[i] - N[i])
    TMDiff.append(TM[i] - N[i])
    NTDiff.append(NT[i] - N[i])
    SADiff.append(SA[i] - N[i])
    NSWDiff.append(NSW[i] - N[i])
    VTDiff.append(VT[i] - N[i])


NWA = np.array(WADiff)
NQL = np.array(QLDiff)
NTM = np.array(TMDiff)
NNT = np.array(NTDiff)
NSA = np.array(SADiff)
NNSW = np.array(NSWDiff)
NVT = np.array(VTDiff)


### write data into the sheet
plt.plot(NWA)
plt.plot(NQL)
plt.plot(NTM)
plt.plot(NNT)
plt.plot(NSA)
plt.plot(NNSW)
plt.plot(NVT)
plt.xlabel('year')
plt.ylabel('wave')
plt.show()



