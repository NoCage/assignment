##Task 3 -Excel via Python
## Create a python script(excel_temp_py)to perform the following tasks:
    # create a new workbook named "World Temperature.xlsx".
    #query the database and calculate the yearly mean temperature of each city in China. Note some data may be missing.
    # Write the relevant data into worksheet you created and saved it.


import openpyxl

import sqlite3

wb = openpyxl.load_workbook('WorldTemperature.xlsx')


sheet = wb.get_sheet_by_name('TemperatureByCity')

"""

wb = openpyxl.workbook('WorldTemperature.xlsx')
sheet = wb.create_sheet('TemperatureByCity')

wb.save('WorldTemperature.xlsx')
print(wb.get_sheet_names())
"""
connection = sqlite3.connect("assignment.db")
cursor = connection.cursor()



result = cursor.execute('select id,avg(AverageTemperature) ,city,date from  GlobalLandTemperaturesByMajorCity where country="China" group by city,strftime("%Y",date) order by city')

id=[]
avgTemp=[]
city=[]
date=[]

for row in  result:
    id.append(row[0])
    avgTemp.append(row[1])
    city.append(row[2])
    date.append(row[3])

for i in range(1,len(id)):
    sheet.cell(row=i, column=1).value = i
    sheet.cell(row=i, column=2).value = avgTemp[i]
    sheet.cell(row=i, column=3).value = city[i]
    sheet.cell(row=i, column=4).value = date[i]



wb.save('WorldTemperature.xlsx')

###bug occur when runing this script while the xlsx file is open , it will pop permission error.

###wb = openpyxl.load_workbook('WorldTemperature.xlsx')
###while creating use wb=openpyxl.Workbook('filename')