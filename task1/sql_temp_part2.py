import sqlite3

connection = sqlite3.connect("assignment.db")
cursor = connection.cursor()


result = cursor.execute('select max(AverageTemperature),min(AverageTemperature),avg(AverageTemperature)  from GlobalLandTemperaturesByState where state = "Queensland" and strftime("%Y",date)="2010"')

for row in result:
    print("Queensland's maximum temperature is",row[0])
    print("Queensland's minimum temperature is",row[1])
    print("Queensland's average temperature is",row[2])

connection.close()