"""
Create a Python script(sql_temp.py)to perform the following tasks:
    * # list  the distinctive major cities located in southern hemisphere ordered by country to the console and
    then write their name,country, and geolocation into a new database table called "Southern cities"

    * Find the maximum,minimum andavearage etmperature of Queensland for year 2010 andprintthis information to the console
"""

import sqlite3

connection = sqlite3.connect("assignment.db")
cursor = connection.cursor()

##create table
"""
sql = ""”
       CREATE TABLE  SouthernCities(
        id INTEGER PRIMARY KEY,  
        City Varchar(20),
        Country Varchar(20),
        Latitude Varchar(20),
        Longitude Varchar(20))
""“

cursor.execute(sql)
"""

result = cursor.execute('select * from GlobalLandTemperaturesByMajorCity order by(Country)')

id=[]
city=[]
country=[]
latitude=[]
longitude=[]
for row in result:
   if row[6][len(row[6])-1] == 'S':
       print(row[0],row[4],row[5],row[6],row[7])
       id.append(row[0])
       city.append(row[4])
       country.append(row[5])
       latitude.append(row[6])
       longitude.append(row[7])


for i in range(1,len(id)):
    sql = "insert into SouthernCities(id,City,Country,Latitude,Longitude) values(?,?,?,?,?)"
    cursor.execute(sql,(id[i],city[i],country[i],latitude[i],longitude[i]))

# list  the distinctive major cities located in southern hemisphere ordered by country to the console

connection.commit()
connection.close()



