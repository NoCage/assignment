####Task 1 - Access the workbooks and create a database

###1. open excel , read data
###2. write to database .

import sqlite3




connection = sqlite3.connect("assignment.db")
cursor = connection.cursor()



sql_command2 = """
       CREATE TABLE  GlobalLandTemperaturesByState(
        id INTEGER PRIMARY KEY, 
        date DATE, 
        AverageTemperature Float(3),
        AverageTemperatureUncertain Float(3),
        State Varchar(20),
        Country Varchar(20))
"""

sql_command = """
        CREATE TABLE  GlobalLandTemperaturesByCountry(
        id INTEGER PRIMARY KEY, 
        date DATE, 
        AverageTemperature Float(3),
        AverageTemperatureUncertain Float(3),
        Country Varchar(20))"""
sql_command1 = """
       CREATE TABLE  GlobalLandTemperaturesByMajorCity(
        id INTEGER PRIMARY KEY, 
        date DATE, 
        AverageTemperature Float(3),
        AverageTemperatureUncertain Float(3),
        City Varchar(20),
        Country Varchar(20),
        Latitude Varchar(20),
        Longitude Varchar(20))
"""



cursor.execute(sql_command)
cursor.execute(sql_command2)

cursor.execute(sql_command1)


