import openpyxl
import sqlite3

##connect to database
connection = sqlite3.connect("assignment.db")
cursor = connection.cursor()
### open excel
wb = openpyxl.load_workbook('GlobalLandTemperaturesByCountry.xlsx')
sheet = wb.get_sheet_by_name('GlobalLandTemperaturesByCountry')
wb1 = openpyxl.load_workbook('GlobalLandTemperaturesByMajorCity.xlsx')
sheet1 = wb1.get_sheet_by_name('GlobalLandTemperaturesByMajorCi')
wb2 = openpyxl.load_workbook('GlobalLandTemperaturesByState.xlsx')
sheet2 = wb2.get_sheet_by_name('GlobalLandTemperaturesByState')


###read data

for i in range(2, sheet.max_row):

    a1 = sheet.cell(row=i, column=1).value
    a2 = sheet.cell(row=i, column=2).value
    if type(a2) == type(None):
        a2 = None
    a3 = sheet.cell(row=i, column=3).value
    if type(a3) == type(None):
        a3 = None
    a4 = sheet.cell(row=i, column=4).value

    sql1 = "INSERT INTO GlobalLandTemperaturesByCountry(id,date,AverageTemperature, AverageTemperatureUncertain, Country) VALUES(?,?,?,?,?)"
    # if
    cursor.execute(sql1, (i - 1, a1, a2, a3, a4))

for i in range(2, sheet1.max_row):

    a1 = sheet1.cell(row=i, column=1).value
    a2 = sheet1.cell(row=i, column=2).value
    if type(a2) == type(None):
        a2 = None
    a3 = sheet1.cell(row=i, column=3).value
    if type(a3) == type(None):
        a3 = None
    a4 = sheet1.cell(row=i, column=4).value
    a5 = sheet1.cell(row=i, column=5).value
    a6 = sheet1.cell(row=i, column=6).value
    a7 = sheet1.cell(row=i, column=7).value

    sql1 = "INSERT INTO GlobalLandTemperaturesByMajorCity(id,date,AverageTemperature, AverageTemperatureUncertain, City,Country,Latitude,Longitude) VALUES(?,?,?,?,?,?,?,?)"
    # if
    cursor.execute(sql1, (i - 1, a1, a2, a3, a4, a5, a6, a7))

for i in range(2,sheet2.max_row):

    a1 = sheet2.cell(row=i,column=1).value
    a2 = sheet2.cell(row=i,column=2).value
    if type(a2)==type(None):
        a2=None
    a3 = sheet2.cell(row=i,column=3).value
    if type(a3)==type(None):
        a3=None
    a4 = sheet2.cell(row=i,column=4).value
    a5 = sheet2.cell(row=i, column=5).value

    sql3 = "INSERT INTO GlobalLandTemperaturesByState(id,date, AverageTemperature, AverageTemperatureUncertain,State, Country) VALUES(?,?,?,?,?,?)"


   # if
    cursor.execute(sql3, (i-1,a1, a2, a3, a4,a5))

connection.commit()
connection.close()

print('reachend')